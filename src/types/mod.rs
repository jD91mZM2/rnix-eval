use anyhow::{anyhow, Context, Result};
use enum_dispatch::enum_dispatch;
use im::hashmap::HashMap;
use log::debug;
use rnix::{
    types::*,
    value::{StrPart, Value as NixValue},
    SyntaxNode,
};

use std::{cell::RefCell, convert::TryFrom, fmt::Debug, iter, rc::Rc};

pub type RcScope = Rc<RefCell<Scope>>;

mod lazy;
pub use lazy::Lazy;

#[derive(Clone, Default, Debug)]
pub struct Scope {
    pub vars: HashMap<String, Lazy>,
}
impl Scope {
    fn eval_keyvalue(&mut self, scope: RcScope, entry: KeyValue) -> Result<(String, Lazy)> {
        let key = entry.key().ok_or(anyhow!("set had no key"))?;
        let val = entry.value().ok_or(anyhow!("set had no value"))?;

        let mut parts = Vec::new();
        for part in key.path() {
            if let Some(ident) = Ident::cast(part.clone()) {
                parts.push(ident.as_str().to_owned());
            } else {
                match self.eval(part)? {
                    _ => unimplemented!("attrsets with this type of key aren't supported yet"),
                }
            }
        }

        assert_eq!(
            parts.len(),
            1,
            "Attributes with a longer path than one are not implemented yet"
        );

        let key = parts.into_iter().next().unwrap();
        Ok((key, Lazy::new_in(scope, val)))
    }

    pub fn eval(&mut self, node: SyntaxNode) -> Result<Value> {
        let kind = ParsedType::try_from(node.clone())
            .map_err(|_| anyhow!("unexpected node type"))
            .unwrap();

        debug!("Evaluating: {:?}", node);

        match kind {
            // Simple types
            ParsedType::Root(wrapper) => {
                self.eval(wrapper.inner().ok_or(anyhow!("root had no content"))?)
            }
            ParsedType::Paren(wrapper) => self.eval(
                wrapper
                    .inner()
                    .ok_or(anyhow!("parentheses had no content"))?,
            ),
            ParsedType::Value(value) => Ok(Value::NixValue(value.to_value()?)),
            ParsedType::Str(string) => {
                let mut result = String::new();
                for part in string.parts() {
                    match part {
                        StrPart::Ast(_ast) => {
                            unimplemented!("string interpolation")
                        }
                        StrPart::Literal(literal) => result.push_str(&literal),
                    }
                }
                Ok(Value::NixValue(NixValue::String(result)))
            }
            ParsedType::Ident(ident) => {
                let lazy = self
                    .vars
                    .get_mut(ident.as_str())
                    .ok_or_else(|| anyhow!("no such variable as {}", ident.as_str()))?;
                Ok(lazy.instantiate()?.clone())
            }

            // Binary Operands
            ParsedType::BinOp(bin_op) => {
                let lhs = bin_op
                    .lhs()
                    .ok_or(anyhow!("binary operand missing left-hand-side"))?;
                let lhs = self.eval(lhs).context("evaluating left-hand-side")?;

                let rhs = bin_op
                    .rhs()
                    .ok_or(anyhow!("binary operand missing left-hand-side"))?;
                let rhs = self.eval(rhs).context("evaluating right-hand-side")?;

                match bin_op.operator() {
                    BinOpKind::Concat => match lhs {
                        Value::NixValue(NixValue::String(lhs)) => match rhs {
                            Value::NixValue(NixValue::String(rhs)) => {
                                Ok(Value::NixValue(NixValue::String(lhs + &rhs)))
                            }
                            _ => unimplemented!("cannot concatenate string with {:?}", lhs),
                        },
                        _ => unimplemented!("cannot yet concatenate lists"),
                    },
                    BinOpKind::Equal => match lhs {
                        Value::NixValue(lhs) => match rhs {
                            Value::NixValue(rhs) => Ok(Value::Bool(lhs == rhs)),
                            _ => unimplemented!("cannot yet compare with rhs {:?}", rhs),
                        },
                        _ => unimplemented!("cannot yet compare with lhs {:?}", lhs),
                    },
                    BinOpKind::Add => match lhs {
                        Value::NixValue(NixValue::Integer(lhs)) => match rhs {
                            Value::NixValue(NixValue::Integer(rhs)) => {
                                Ok(Value::NixValue(NixValue::Integer(lhs.wrapping_add(rhs))))
                            }
                            _ => unimplemented!("cannot yet subtract with rhs {:?}", rhs),
                        },
                        _ => unimplemented!("cannot yet subtract with lhs {:?}", lhs),
                    },
                    BinOpKind::Sub => match lhs {
                        Value::NixValue(NixValue::Integer(lhs)) => match rhs {
                            Value::NixValue(NixValue::Integer(rhs)) => {
                                Ok(Value::NixValue(NixValue::Integer(lhs.wrapping_sub(rhs))))
                            }
                            _ => unimplemented!("cannot yet subtract with rhs {:?}", rhs),
                        },
                        _ => unimplemented!("cannot yet subtract with lhs {:?}", lhs),
                    },
                    op => unimplemented!("cannot yet perform operation {:?}", op),
                }
            }

            // Scopes
            ParsedType::LetIn(let_in) => {
                let subscope = Rc::new(RefCell::new(self.clone()));

                let mut keys = Vec::new();

                // Push all entries
                for entry in let_in.entries() {
                    let (key, val) = self.eval_keyvalue(Rc::clone(&subscope), entry)?;
                    subscope.borrow_mut().vars.insert(key.clone(), val);
                    keys.push(key);
                }

                let body = let_in.body().ok_or(anyhow!("let..in had no body"))?;
                let mut subscope_clone = subscope.borrow().clone();
                subscope_clone.eval(body)
            }

            // Sets
            ParsedType::AttrSet(attrs) => {
                let mut value = ResolvedAttrSet::default();

                for entry in attrs.entries() {
                    let (key, val) =
                        self.eval_keyvalue(Rc::new(RefCell::new(self.clone())), entry)?;
                    value.entries.insert(key, val);
                }

                Ok(Value::AttrSet(value))
            }
            ParsedType::Select(select) => {
                let set = select.set().ok_or(anyhow!("select missing set"))?;
                let set = self.eval(set)?;

                let index = select.index().ok_or(anyhow!("select missing index"))?;
                let index_str = if let Some(index) = Ident::cast(index) {
                    index.as_str().to_owned()
                } else {
                    unimplemented!("dynamic set indexing")
                };

                if let Value::AttrSet(set) = set {
                    let value = set
                        .entries
                        .get(&index_str)
                        .ok_or(anyhow!("set did not have attr {:?}", index_str))?;

                    value.clone().into_value()
                } else {
                    Err(anyhow!("trying to index a non-set value {:?}", set))
                }
            }

            // Functions
            ParsedType::Lambda(lambda) => {
                let body = lambda.body().ok_or(anyhow!("lambda missing body"))?;

                if let Some(ident) = lambda.arg().and_then(Ident::cast) {
                    Ok(Value::Lambda(ResolvedLambda {
                        argname: ident.as_str().to_owned(),

                        parent_scope: self.clone(),
                        body: body,
                    }))
                } else {
                    unimplemented!("lambda with pattern arg")
                }
            }
            ParsedType::Apply(apply) => {
                let lambda = apply.lambda().ok_or(anyhow!("apply missing lambda"))?;
                let lambda = self.eval(lambda).context("evaluating lambda to apply")?;

                let arg = apply.value().ok_or(anyhow!("apply missing argument"))?;
                let arg = Lazy::new_in(Rc::new(RefCell::new(self.clone())), arg);

                if let Value::Lambda(lambda) = lambda {
                    let lazy = lambda.invoke(arg);
                    Ok(lazy.into_value()?)
                } else {
                    Err(anyhow!(
                        "tried to invoke {:?} which is not a lambda",
                        lambda
                    ))
                }
            }

            // Conditionals
            ParsedType::IfElse(if_else) => {
                let condition = if_else
                    .condition()
                    .ok_or(anyhow!("if..else missing condition"))?;
                let condition = self.eval(condition).context("evaluating condition")?;

                match condition {
                    Value::Bool(boolean) => {
                        let body = if boolean {
                            if_else.body().ok_or(anyhow!("missing body"))?
                        } else {
                            if_else.else_body().ok_or(anyhow!("missing body"))?
                        };
                        self.eval(body)
                    }
                    _ => Err(anyhow!("expected boolean condition, found {:?}", condition)),
                }
            }

            _ => unimplemented!("cannot yet evaluate this type: {:?}", node.kind()),
        }
    }
}

#[enum_dispatch]
pub trait Evaluable {
    /// List all evaluatable children
    fn children(&mut self) -> Box<dyn Iterator<Item = &mut Lazy> + '_>;

    // Evaluate all children recursively
    fn instantiate_all(&mut self) -> Result<()> {
        for lazy in self.children() {
            let value = lazy.instantiate()?;
            value.instantiate_all()?;
        }
        Ok(())
    }
}

impl Evaluable for NixValue {
    fn children(&mut self) -> Box<dyn Iterator<Item = &mut Lazy> + '_> {
        Box::new(iter::empty())
    }
}
impl Evaluable for bool {
    fn children(&mut self) -> Box<dyn Iterator<Item = &mut Lazy> + '_> {
        Box::new(iter::empty())
    }
}

mod attrset;
mod lambda;

pub use self::{attrset::AttrSet as ResolvedAttrSet, lambda::Lambda as ResolvedLambda};

#[enum_dispatch(Evaluable)]
#[derive(Debug, Clone)]
pub enum Value {
    AttrSet(ResolvedAttrSet),
    Bool(bool),
    Lambda(ResolvedLambda),
    NixValue(NixValue),
}
