use super::{Evaluable, Lazy};

use im::hashmap::HashMap;

#[derive(Debug, Default, Clone)]
pub struct AttrSet {
    pub entries: HashMap<String, Lazy>,
}

impl Evaluable for AttrSet {
    fn children(&mut self) -> Box<dyn Iterator<Item = &mut Lazy> + '_> {
        Box::new(self.entries.iter_mut().map(|(_k, v)| v))
    }
}
