use anyhow::Result;
use rnix::SyntaxNode;

use super::{RcScope, Scope, Value};

use std::{
    cell::RefCell,
    fmt::{self, Debug},
    rc::Rc,
};

#[derive(Debug, Clone)]
enum Repr {
    Evaluated(Value),
    Todo(RcScope),
}

/// A value that may not have been instantiated yet
#[derive(Clone)]
pub struct Lazy {
    repr: Repr,
    node: SyntaxNode,
}
impl Lazy {
    /// Create a new lazy value from rnix node, to be evaluated in the given scope
    pub fn new(node: SyntaxNode) -> Self {
        Self::new_in(Rc::new(RefCell::new(Scope::default())), node)
    }

    /// Create a new lazy value from rnix node, to be evaluated in the given scope
    pub fn new_in(scope: RcScope, node: SyntaxNode) -> Self {
        Self {
            node,
            repr: Repr::Todo(scope),
        }
    }

    /// Create a new pre-evaluated value originating from the given SyntaxNode
    pub fn evaluated(value: Value, node: SyntaxNode) -> Self {
        Self {
            node,
            repr: Repr::Evaluated(value),
        }
    }

    /// Instantiate node, resolving its type and children
    pub fn instantiate(&mut self) -> Result<&mut Value> {
        match self.repr {
            Repr::Evaluated(ref mut value) => Ok(value),
            Repr::Todo(ref mut scope) => {
                let value = scope.borrow_mut().eval(self.node.clone())?;
                self.repr = Repr::Evaluated(value);
                match self.repr {
                    Repr::Evaluated(ref mut value) => Ok(value),
                    Repr::Todo(_) => unreachable!(),
                }
            }
        }
    }

    pub fn into_value(mut self) -> Result<Value> {
        self.instantiate()?;
        match self.repr {
            Repr::Evaluated(value) => Ok(value),
            Repr::Todo(_) => unreachable!("called instantiate, should be evaluated"),
        }
    }
}
impl Debug for Lazy {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self.repr {
            Repr::Evaluated(ref value) => value.fmt(f),
            Repr::Todo(_) => self.node.fmt(f),
        }
    }
}
