use super::{Evaluable, Lazy, Scope};
use rnix::SyntaxNode;

use std::{cell::RefCell, iter, rc::Rc};

#[derive(Debug, Clone)]
pub struct Lambda {
    pub argname: String,

    pub parent_scope: Scope,
    pub body: SyntaxNode,
}

impl Lambda {
    pub fn invoke(self, arg: Lazy) -> Lazy {
        let mut subscope = self.parent_scope;
        subscope.vars.insert(self.argname, arg);

        Lazy::new_in(Rc::new(RefCell::new(subscope)), self.body)
    }
}

impl Evaluable for Lambda {
    fn children(&mut self) -> Box<dyn Iterator<Item = &mut Lazy> + '_> {
        Box::new(iter::empty())
    }
}
