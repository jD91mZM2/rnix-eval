use anyhow::{anyhow, Result};

use std::{env, fs};

use nix_eval::{Evaluable, NixFile};

fn main() -> Result<()> {
    env_logger::init();

    let filepath = env::args()
        .skip(1)
        .next()
        .ok_or(anyhow!("missing filepath argument"))?;

    let data = fs::read_to_string(filepath)?;

    let mut root = NixFile::parse(data).value();
    let root_value = root.instantiate()?;

    println!("{:?}", root_value);

    root_value.instantiate_all()?;

    println!("{:#?}", root_value);
    Ok(())
}
