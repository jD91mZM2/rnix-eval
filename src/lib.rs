use rnix::AST;

mod types;

pub use self::types::*;

pub struct NixFile {
    tree: AST,
}
impl NixFile {
    pub fn parse<T>(text: T) -> Self
    where
        T: AsRef<str>,
    {
        let tree = rnix::parse(text.as_ref());

        Self { tree }
    }

    pub fn value(&self) -> Lazy {
        Lazy::new(self.tree.node())
    }
}
