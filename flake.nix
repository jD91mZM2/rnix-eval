{
  description = "A rust program";

  inputs = {
    utils.url = "github:numtide/flake-utils";
    naersk.url = "github:nmattia/naersk";
    mozillapkgs = {
      url = "github:mozilla/nixpkgs-mozilla";
      flake = false;
    };
  };

  outputs = { self, nixpkgs, utils, naersk, mozillapkgs }:
    utils.lib.eachDefaultSystem (system: let
      pkgs = nixpkgs.legacyPackages."${system}";

      naersk-lib = naersk.lib."${system}";

      nativeBuildInputs = with pkgs; [ /* pkgconfig */ ];
      buildInputs = with pkgs; [ /* openssl */ ];
    in rec {
      # `nix build`
      packages.nix-eval = naersk-lib.buildPackage {
        pname = "nix-eval";
        root = ./.;

        inherit nativeBuildInputs buildInputs;
      };
      defaultPackage = packages.nix-eval;

      # `nix run`
      apps.nix-eval = utils.lib.mkApp {
        drv = packages.nix-eval;
      };
      defaultApp = apps.nix-eval;

      # `nix develop`
      devShell = pkgs.mkShell {
        buildInputs = buildInputs;
        nativeBuildInputs = nativeBuildInputs ++ (with pkgs; [ rustc cargo ]);
      };
    });
}
