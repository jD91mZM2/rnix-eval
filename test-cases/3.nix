let
  count = val: {
    head = val;
    tail = count (val + 1);
  };
  index = list: val:
    if val == 0 then
      list.head
    else
      index list.tail (val - 1);
in
index (count 0) 5
