let
  makeUser = firstname: lastname: {
    FirstName = firstname;
    LastName = lastname;
    Name = firstname ++ " " ++ lastname;
  };
in
{
  john_doe = makeUser "John" "Doe";
  jane_doe = makeUser "Richard" "Roe";
}
